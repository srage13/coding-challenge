const config = module.exports = {
    dbname : process.env.DB_NAME,
    host: process.env.DB_HOST,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    port: process.env.PORT,
    secrets: {
        jwtUserSalt: process.env.JWT_USER_SALT
    }
}