const Sequelize = require('sequelize');
const config = require('./config');

const sequelize = new Sequelize(config.dbname, config.username, config.password, {
    dialect: 'mysql',
    host: config.host,
    timestamps: false
});

module.exports = sequelize;