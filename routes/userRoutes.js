const express = require('express');
const router = express.Router();
const userAuth = require('../middleware/userAuth');
const Logger = require('../lib/logger');
const userController = require('../controller/userController');
const { body } = require('express-validator/check');

const logger = new Logger();

const ADMIN_ROLE = 1;
const USER_ROLE = 2;

router.post('/register', [
    body('email').isEmail().withMessage('Please enter a valid email.').normalizeEmail(),
    body('password').trim().isLength({ min: 8 }),
    body('firstname').trim().not().isEmpty(),
    body('lastname').trim().not().isEmpty(),
    body('role').trim().not().isEmpty().isIn([1, 2]),
], userController.register);

router.post('/login', [
    body('email').isEmail().withMessage('Please enter a valid email.').normalizeEmail(),
    body('password').trim().isLength({ min: 8 })
], userController.login);

router.get('/', userController.getUsers);

module.exports = router;