const express = require('express');
const router = express.Router();
const userAuth = require('../middleware/userAuth');
const Logger = require('../lib/logger');
const ticketController = require('../controller/ticketController');
const { body, param, check } = require('express-validator/check');

// Hard coded for now need to move to a DB table
const TICKET_TYPES = [
    'enhancement',
    'bug fix',
    'development',
    undefined
];
const COMPLEXITY = [
    'low',
    'mid',
    'high',
    undefined
];

const ticketValidation = [
    body('summary').trim().not().isEmpty(),
    body('description').trim(),
    body('type').trim().not().isEmpty().isIn(TICKET_TYPES),
    body('complexity').trim().not().isEmpty().isIn(COMPLEXITY),
    body('estimatedtime').trim().not().isEmpty(),
    body('cost').trim().not().isEmpty(),
];

router.post('/', userAuth, ticketValidation, ticketController.addTicket);

router.get('/list', userAuth ,ticketController.getTicketList);

router.put('/edit/:id', userAuth,  [
    body('summary').trim(),
    body('description').trim(),
    body('type').trim().isIn(TICKET_TYPES),
    body('complexity').trim().isIn(COMPLEXITY),
    body('estimatedTime').trim(),
    body('cost').trim(),
],ticketController.editTicket);

router.put('/edit/:id/:action', userAuth, [
    param('id').isInt(),
    param('action').isIn(["reject", "approve" ])
], ticketController.updateTicketStatus);

module.exports = router;