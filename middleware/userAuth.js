const jwt = require('jsonwebtoken');
const config = require('../config/config');
const User = require('../model/user');
const Logger = require('../lib/logger');

const logger = new Logger();

module.exports = (req, res, next) => {
    const authHeader = req.get('Authorization');
    if (!authHeader) {
        const error = new Error('Not authenticated.');
        logger.error(error + " Accessed by " + req.ip);
        return res.sendStatus(401);
    }
    const token = authHeader.split(' ')[1];
    let decodedToken;
    try {
        // Check whether the token provided by the request is valid.
        decodedToken = jwt.verify(token, config.secrets.jwtUserSalt);
    } catch (err) {
        err.statusCode = 500;
        logger.error(err);
        return res.sendStatus(500);
    }
    if (!decodedToken) {
        const error = new Error('Not authenticated.');
        logger.error(error + " Accessed by " + req.ip);
        return res.sendStatus(401);
    }

    // Check whether the userId present in the token actually exists in the DB
    User.count({
        where: {
            userId: decodedToken.userId
        }
    })
    .then(count => {
        if (count !== 1) {
            logger.error("Not Authorized. Accessed by " + req.ip);
            return res.sendStatus(401);
        } else {
            req.userId = decodedToken.userId;
            next();
        }
    })
    .catch(err => logger.info(err));
};
