const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
require('dotenv').config();
const db = require('./config/dbconfig');
const Logger = require('./lib/logger');

const app = express();


app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

const logger = new Logger();

// Routes

app.use('/api/user', require('./routes/userRoutes.js'));
app.use('/api/ticket', require('./routes/ticketRoutes.js'));


// Check if the database is connected
db.authenticate()
.then(() => logger.info("Connected to database"))
.catch(err => logger.info(err));


const PORT = process.env.PORT || 4000;

// Start server on a port

app.listen(PORT, logger.info(`Server started on PORT ${PORT}`));