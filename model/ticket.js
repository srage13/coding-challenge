const Sequelize = require('sequelize');
const db = require('../config/dbconfig');
const User = require('./user');

const ticket = db.define('ticket', {
  ticketId: {
    type: Sequelize.INTEGER(11),
    allowNull: false,
    primaryKey: true,
    autoIncrement: true
  },
  summary: {
    type: Sequelize.TEXT(50),
    allowNull: false
  },
  description: {
    type: Sequelize.TEXT(100),
    allowNull: false
  },
  type: {
    type: Sequelize.CHAR(50),
    allowNull: false
  },
  complexity: {
    type: Sequelize.CHAR(50),
    allowNull: false
  },
  estimatedTime : {
    type: Sequelize.CHAR(20),
    allowNull: false
  },
  cost : {
    type: Sequelize.INTEGER(10),
    allowNull: true
  },
  status : {
    type: Sequelize.INTEGER(2),
    allowNull: true
  },
  userId: {
    type: Sequelize.INTEGER(11),
    allowNull: true,
    references: {
      model: 'user',
      key: 'userId'
    }
  }
}, {
  tableName: 'ticket',
});

ticket.getTicketsForUser = (id) => {
  return ticket.findAll({
      where: {
          userId: id
      }
  })
  .then(tickets => {
      return tickets;
  })
  .catch(err => logger.info(err));
}

ticket.getAllTickets = (id) => {
  return ticket.findAll()
  .then(tickets => {
      return tickets;
  })
  .catch(err => logger.info(err));
}


module.exports = ticket;