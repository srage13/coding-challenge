const Sequelize = require('sequelize');
const db = require('../config/dbconfig');
const user = require('./user');


const role = db.define('role', {
  roleId: {
    type: Sequelize.INTEGER(1),
    allowNull: false,
    primaryKey: true,
    autoIncrement: true
  },
  title: {
    type: Sequelize.CHAR(50),
    allowNull: false
  }
}, {
  tableName: 'role',
  timestamps: false
});

module.exports = role;