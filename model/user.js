const Sequelize = require('sequelize');
const db = require('../config/dbconfig');
const role = require('./role');

const ADMIN_ROLE = 1;

const user = db.define('user', {
  userId: {
    type: Sequelize.INTEGER(11),
    allowNull: false,
    primaryKey: true,
    autoIncrement: true
  },
  firstName: {
    type: Sequelize.STRING(100),
    allowNull: false
  },
  lastName: {
    type: Sequelize.STRING(100),
    allowNull: false
  },
  email: {
    type: Sequelize.STRING(100),
    allowNull: false
  },
  password: {
    type: Sequelize.STRING(255),
    allowNull: false
  },
  roleId: {
    type: Sequelize.INTEGER(1),
    allowNull: true,
    references: {
      model: 'role',
      key: 'roleId'
    }
  }
}, {
  tableName: 'user',
});

user.checkIfAdmin = async (id) => {
    return user.findOne({where: { userId: id}})
            .then(thisUser => {
                if(thisUser.roleId == ADMIN_ROLE) return true;
                return false;
            });
}

user.hasOne(role, {foreignKey: 'roleId', sourceKey: 'roleId'});
role.hasMany(user, {foreignKey: 'roleId'});

module.exports = user;