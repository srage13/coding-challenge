const Logger = require('../lib/logger');
const User = require('../model/user');
const Ticket = require('../model/ticket');
const { validationResult } = require('express-validator/check');

const STATUS_AWATING_APPROVAL = 0;
const STATUS_APPROVED = 1;
const STATUS_REJECTED = 2;

const logger = new Logger();

exports.getTicketList = (req, res, next) => {
    User.checkIfAdmin(req.userId).then(isAdmin => {
        if(!isAdmin) {
            Ticket.getTicketsForUser(req.userId).then(tickets => {
                res.status(200).send(tickets);
            });
        } else {
            Ticket.getAllTickets(req.userId).then(tickets => {
                res.status(200).send(tickets);
            });
        }
    });
}

exports.addTicket = (req, res,next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        logger.error("Validation failed");
        return res.status(422).send({success: false, errorMessage: "Validation failed"});
    }
    Ticket.create({
        summary: req.body.summary,
        description: req.body.description,
        type: req.body.type,
        complexity: req.body.complexity,
        estimatedTime: req.body.estimatedTime,
        cost: req.body.cost,
        status: STATUS_AWATING_APPROVAL,
        userId: req.userId,
    })
    .then((ticket) => {
        const returnData = {
            ticket ,success: true
        }
        logger.info(`Created ticket with ID ${ticket.ticketId}`);
        res.status(201).send(returnData);
    })
    .catch(err => logger.info(err));
}

exports.editTicket = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        console.log(errors);
        logger.error("Validation failed");
        return res.status(422).send({success: false, errorMessage: "Validation failed"});
    }
    const ticketId = req.params.id;
    User.checkIfAdmin(req.userId).then(isAdmin => {
        if(isAdmin) {
            Ticket.findOne({
                where: {
                    ticketId: ticketId
                }
            })
            .then(thisTicket => {
                const {summary, description, type, complexity, estimatedtime, cost, status} = req.body;
                let newCost = typeof cost === undefined || cost == '' ? thisTicket.cost : cost
                let newComplexity = typeof complexity === undefined || complexity == '' ? thisTicket.complexity : complexity
                let newType = typeof type === undefined || type == '' ? thisTicket.type : type
                let newSummary = typeof type === undefined || type == '' ? thisTicket.summary : summary
                let newDescription = typeof type === undefined || type == '' ? thisTicket.description : description
                thisTicket.update({
                    summary: newSummary, description: newDescription, type: newType, complexity: newComplexity, estimatedTime: estimatedtime, cost: newCost, status:status
                })
                .then(updatedTicket => {
                    res.status(200).send(updatedTicket);
                });
            });
        } else {
            return res.sendStatus(401);
        }
    });
}

exports.updateTicketStatus = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        logger.error("Validation failed");
        return res.status(422).send({success: false, errorMessage: "Validation failed"});
    }
    const ticketId = req.params.id;
    const action = req.params.action;
    User.checkIfAdmin(req.userId).then(isAdmin => {
        if(isAdmin) {
            let status = action == 'approve' ? STATUS_APPROVED : STATUS_REJECTED
            Ticket.findOne({
                where: {
                    ticketId: ticketId
                }
            })
            .then(thisTicket => {
                thisTicket.update({
                    status
                })
                .then(updatedTicket => {
                    res.status(200).send(updatedTicket);
                });
            });
        } else {
            return res.sendStatus(401);
        }
    });
}