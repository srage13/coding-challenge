const Logger = require('../lib/logger');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const User = require('../model/user');
const Role = require('../model/role');
const config = require('../config/config');
const { validationResult } = require('express-validator/check');

const logger = new Logger();

exports.register = (req, res,next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        console.log(errors);
        logger.error("Validation failed");
        return res.status(422).send({success: false, errorMessage: "Validation failed"});
    }
    let accessToken = null;
    let userId = null;
    let encPassword = bcrypt.hashSync(req.body.password, 10);

    const checkIfUserExists = () => {
        return User.count({ where: { email: req.body.email } })
        .then(count => {
            if (count == 1) return true;
            else return false;
        });
    }

    checkIfUserExists().then(userExists => {
        if(!userExists) {
            User.create({
                email: req.body.email,
                firstName: req.body.firstname,
                lastName: req.body.lastname,
                roleId: req.body.role,
                password: encPassword,
            })
            .then((user) => {
                Role.findOne({
                    where: {
                        roleId: user.roleId
                    }
                }).then(thisRole => {
                    const { userId, firstName, lastName } = user;
                    const role = thisRole.title;
                    accessToken = jwt.sign({ userId: userId }, config.secrets.jwtUserSalt);
                    const returnData = {
                        userId, accessToken, firstName, lastName, role ,success: true
                    }
                    logger.info(`Created user with ID ${userId}`)
                    res.status(201).send(returnData);
                });
            })
            .catch(err => logger.info(err));
        } else {
            res.status(200).send({success: false, errorMessage: 'User already exists'});
        }
    });
}

exports.login = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        console.log(errors);
        logger.error("Validation failed");
        return res.status(422).send({success: false, errorMessage: "Validation failed"});
    }

    let accessToken = null;
    let returnData = null;

    User.findOne({
        where: {
            email: req.body.email
        },
        include: [Role]
    })
    .then(user => {
        const { userId, firstName, lastName } = user;
        const role = user.role.title;
        bcrypt.compare(req.body.password, user.password, function(err, result) {
            if (result) {
                accessToken = jwt.sign({ userId:userId }, config.secrets.jwtUserSalt);
                returnData = {
                    userId, accessToken, firstName, lastName, role, success: true
                }
                logger.info(`User with ID ${userId} logged in`);
                res.status(200).send(returnData);
            } else {
                returnData = {
                    success: false, errorMessage: 'Could not login, you may have entered an incorrect email or password'
                }
                logger.info(`Unsuccessfull login attempt.`);
                res.status(200).send(returnData);
            }
            if(err) {
                returnData = {
                    success: false, errorMessage: "Could not login, Please try again"
                }
                logger.error(`Could not login | error | ${err}`);
                res.status(500).send(returnData);
            }
        });
    })
    .catch(err => logger.info(err));
}

exports.getUsers = (req, res, next) => {
    User.getAllUsers().then(users => {
        res.send(users);
    });
}