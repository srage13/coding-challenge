# This is a server application

## Requirnments

- Node - v10.16.2
- NPM - v6.9.0
- MySql - v5.7.26^

## Installation and Usage guide

- Run `npm install` to install all dependencies for the application.

- Run the `initial_db.sql` found in `schema` to setup the DB for the application. Create a database and run the sql file.

- Create a `.env` file using the `.env_example` file as a guide this file contains the environment variables such as the host, dbname, passwords, etc.

- Once the `.env` file has been configured you can run the sever using any one of the following commands
    - `npm run dev` - This use's nodemon which is installed via npm to run the server and canbe used for development environments
    - `npm run prod` - To run this command you need to first install PM2 as a global dependency, run the command `npm install pm2 -g`, Once the package has been installed you can run the `npm run prod` command to run the application via PM2. Use this for running the application in a production environment.


## Asumptions 

- I have used the roles as a table in the Database.


## Usage


`POST` /api/user/login -

Description -
    Authenticates the user.

Parameters - 
    email - string,
    password - string

Response - 
    userId - ID of the user logged in,
    accessToken - AUTH_TOKEN,
    role - Role of the user Admin|User,
    firstName - User's First name,
    lastName: User's Last name



`POST` /api/user/register -

Description -
    Registers a new user.

Parameters - 
    firstname - string,
    lastname - string,
    role - 1 - Admin, 2 - User,
    email - string,
    password - string

Response - 
    userId - ID of the user logged in,
    accessToken - AUTH_TOKEN,
    role - Role of the user Admin|User,
    firstName - User's First name,
    lastName: User's Last name 

    

`POST` /api/ticket -


Description -
    Adds a new ticket. By the logged in user.

Authorization - 
    ACCESS_TOKEN

Parameters - 
    summary - string,
    description - string,
    type - string - enhancement | bugfix | development | qa,
    complexity - string - low | mid | high,
    estimatedTime - string,
    cost - float,
    status - int 0: awaiting approval | new, 1: Approved, 2: Rejected ,

Response - 
    summary - Summary of the ticket,
    description - Description of the ticket,
    type - Type of the ticket,
    complexity: Complexity of the ticket,
    estimatedTime: Estimated time for resolving the ticket,
    cost: Cost,
    status: Approval status of the ticket,



`PUT` /api/ticket/{:id} -


Description -
    Edits the ticket, Only accessible by admin.

Authorization - 
    ACCESS_TOKEN

Parameters - 
    id - int - Ticket Id,
    summary - string,
    description - string,
    type - string - enhancement | bugfix | development | qa,
    complexity - string - low | mid | high,
    estimatedTime - string,
    cost - float,
    status - int 0: awaiting approval | new, 1: Approved, 2: Rejected ,

Response - 
    summary - Summary of the ticket,
    description - Description of the ticket,
    type - Type of the ticket,
    complexity: Complexity of the ticket,
    estimatedTime: Estimated time for resolving the ticket,
    cost: Cost,
    status: Approval status of the ticket,


`GET` /api/ticket/list -

Description -
    Returns the tickets made by the user, And return all the tickets is Admin is logged in.

Authorization - 
    ACCESS_TOKEN

Response - 
    summary - Summary of the ticket,
    description - Description of the ticket,
    type - Type of the ticket,
    complexity: Complexity of the ticket,
    estimatedTime: Estimated time for resolving the ticket,
    cost: Cost,
    status: Approval status of the ticket,



`PUT` /api/ticket/{:id}/{:action} -


Description -
    Edits the ticket, Only accessible by admin.

Authorization - 
    ACCESS_TOKEN

Parameters
    id - int - Ticket Id,
    action - action - approve|reject

Response - 
    summary - Summary of the ticket,
    description - Description of the ticket,
    type - Type of the ticket,
    complexity: Complexity of the ticket,
    estimatedTime: Estimated time for resolving the ticket,
    cost: Cost,
    status: Approval status of the ticket,
